# Anbox Debian package

This repo contains the debian file for Anbox https://github.com/anbox/anbox/ .

## Build

```
mkdir anbox-debian && cd anbox-debian
git clone https://github.com/anbox/anbox.git
git clone https://gitlab.com/gobudgie/anbox.git anbox-debian
cp -r anbox-debian/debian anbox/

cd anbox

dpkg-buildpackage -rfakeroot -b
```